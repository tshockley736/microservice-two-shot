import React, { useEffect, useState } from 'react';

function ShoeForm(props) {
    const [bins, setBins] = useState([])

    const [formData, setFormData] = useState({
        manufacturer: '',
        name: '',
        color: '',
        picture_url: '',
        bin: '',
    })

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                name: '',
                color: '',
                picture_url: '',
                bin: '',
            });

            window.location.href = "/shoes"
        }
    }


    const fetchBins = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {fetchBins();}, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Pair of Shoes to your Wardrobe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" autoComplete="off"/>
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.picture_url} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.bin} required name="bin" id="bin" className="form-select">
                        <option value="">Choose A Wardrobe Bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                    Closet Name: {bin.closet_name} - Bin #:{bin.bin_number} - Bin Size:{bin.bin_size}
                                </option>
                            );
                        })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Add</button>
                </form>
            </div>
            </div>
        </div>
    )
}

export default ShoeForm;
