import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hatDetailList, setHatDetailList] = useState([])

    async function getHats() {
        const url= 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const hatDetailList = []

                for (const hatResponse of responses) {
                    if (hatResponse.ok) {
                        const hatDetails = await hatResponse.json()
                        hatDetailList.push(hatDetails)
                    } else {
                        console.error(hatResponse);
                    }
                }
                setHatDetailList(hatDetailList)
        }
    } catch (e) {
        console.error(e);
    }
    }

    useEffect(() => {
        getHats();
    }, []);

    const handleDelete = async (e) => {
        const href = e.target.value

        fetch(`http://localhost:8090${href}`, {method:"delete"})

        window.location.href = "/hats"
    }

    return (
        <>
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                    Need to keep track of your shoes and hats? We have
                    the solution for you!
                    </p>
                </div>
            </div>
            <div className="container">
                <h2 className="text-center">Hats</h2>
                <div className="row">
                    {hatDetailList.map((hatDetail) => {
                        return (
                            <div key={hatDetail.href} className='col-3'>
                                <div className="card mb-3 shadow">
                                    <img
                                        src={hatDetail.picture_url}
                                        className="card-img-top"
                                        alt='Picture of hat'
                                    />
                                    <div className="card-body">
                                        <h6 className="card-title">Style Name: {hatDetail.style_name}</h6>
                                        <h6 className="card-subtitle mb-2">Color: {hatDetail.color}</h6>
                                        <h6 className="card-subtitle mb-2">Fabric: {hatDetail.fabric}</h6>
                                    </div>
                                    <div className="card-footer">
                                        <div><h6>Closet Name: {hatDetail.location.closet_name}</h6></div>
                                        <div><h6>Section #: {hatDetail.location.section_number}</h6></div>
                                        <div><h6>Shelf #: {hatDetail.location.shelf_number}</h6></div>
                                        <div className="text-center">
                                        <button className="btn btn-primary" value={hatDetail.href} onClick={handleDelete}>Delete Hat</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export default HatsList;
