 import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ShoesList() {
    const [shoeDetailList, setShoeDetailList] = useState([])

    async function getShoes() {
        const url = 'http://localhost:8080/api/shoes/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080${shoe.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const shoeDetailList = [];

                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const shoeDetails = await shoeResponse.json()
                        shoeDetailList.push(shoeDetails)
                    } else {
                        console.error(shoeResponse);
                    }
                }
                setShoeDetailList(shoeDetailList)
            }
        } catch (e) {
            console.error(e);
        }
    }

    useEffect(() => {
        getShoes();
    }, []);


    const handleDelete = async (e) => {
        const href = e.target.value

        fetch(`http://localhost:8080${href}`,{method:"delete"})

        window.location.href = "/shoes"
    }



    return (
        <>
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                    Need to keep track of your shoes and hats? We have
                    the solution for you!
                    </p>
                </div>
            </div>
            <div className="container">
                <h2 className="text-center">Shoes</h2>
                <div className="row">
                    {shoeDetailList.map((shoeDetail) => {
                        return (
                            <div key={shoeDetail.href} className='col-3'>
                                <div className="card mb-3 shadow">
                                        <img
                                            src={shoeDetail.picture_url}
                                            className="card-img-top"
                                            alt='Picture of Shoe Pair'
                                        />
                                    <div className="card-body">
                                        <h6 className="card-title">Manufacturer: {shoeDetail.manufacturer}</h6>
                                        <h6 className="card-subtitle mb-2">Name: {shoeDetail.name}</h6>
                                        <h6 className="card-subtitle mb-2">Color: {shoeDetail.color}</h6>
                                    </div>
                                    <div className="card-footer">
                                        <div><h6>Closet Name: {shoeDetail.bin.closet_name}</h6></div>
                                        <div><h6>Bin #: {shoeDetail.bin.bin_number}</h6></div>
                                        <div><h6>Bin Size: {shoeDetail.bin.bin_size}</h6></div>
                                        <div className="text-center">
                                            <button className="btn btn-primary" value={shoeDetail.href} onClick={handleDelete}>Delete Shoe</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );
}

export default ShoesList;
