# Wardrobify

Team:

* Billy Berger-Bailey - Hats
* Taylor Shockley- Shoes

## [Design]![alt text](<Wardrobify Diagram.png>)

## Shoes microservice

For the shoes microservice, we have two models.  One that reflects a shoe pair entity,  and one that reflects a bin value object where we store our shoes in our wardrobe.  Our BinVO model and the associated database reflects the Bin model and database in our Wardrobe microservice by polling every 60 seconds for new data.  Any time a new instance of a Bin is created (or updated) in the Wardrobe microservice database, within 60 seconds, our poller in our Shoes microservice updates or creates an instance a BinVO in our Shoes microservice database.  On our frontend, our clients are able to fill out forms to add shoes to their bins in their wardrobe, as well as see a list of all pairs of shoes that they've added.

## Hats microservice

For our Hats microservice, the functionality is very similar to our shoes microservice where we have one model that represents a location value object and one model that represents a hat entity. The LocationVO model as we have named it reflects the location model and associated database in our wardrobe microservice. Inside of our Hats microservice we have created a poller that polls every 60 seconds to detect a new location object inm our wardrobe microservice which in turn creates a new locationVO in our hats microservice. On our frontend, our clients are able to fill out forms to add hats to locations in their wardrobe, as well as see a list of all of the hats that they've added.
